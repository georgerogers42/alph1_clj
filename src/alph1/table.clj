(ns alph1.table
  (:import [java.io Writer BufferedReader]
           [java.util.regex Pattern])
  (:require [clojure.java.io :as io])
  (:gen-class))

(set! *warn-on-reflection* true)

(defn #^String capitalize [#^String word]
  (cond
    (= word "") ""
    :else       (str (.toUpperCase (.substring word 0 1))
                     (.substring word 1))))

(defn printflush
  ([#^Writer w a]
   (.write w (str a))
   (.flush w))
  ([a]
   (printflush *out* a)))

(def ^:private #^Pattern wordsplit #"\W+")
(defn build-table [table #^BufferedReader r]
  (loop [table table]
    (if-let [line (.readLine r)]
      (recur (reduce (fn [table word]
                       (if-not (= word "")
                         (let [word (capitalize word)]
                           (assoc table word (inc (get table word 0))))
                         table))
                     table
                     (.split wordsplit line)))
      table)))

(defn report [w table o]
  (doseq [[k v] (sort o table)]
    (printflush w (format "%24s: %10s\n" k v))))

(defn by-first [a b]
  (< (compare (first a) (first b)) 0))

(defn by-second [a b]
  (< (compare (second a) (second b)) 0))
