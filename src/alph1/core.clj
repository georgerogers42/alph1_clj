(ns alph1.core
  (:import [java.io Writer BufferedReader]
           [java.util.regex Pattern])
  (:require [clojure.java.io :as io]
            [alph1.table :as tbl])
  (:gen-class))

(set! *warn-on-reflection* true)

(defn -main
  [& args]
  (let [o (cond
            (and (> (count args) 0)
                 (= (first args) "-a")) tbl/by-first
            :else                       tbl/by-second)]
    (doseq [i (range 1 101)]
      (tbl/printflush (format "# Begin Iteration %d\n" i))
      (let [table (with-open [f (io/reader "TEXT-PCE-127.txt")]
                    (tbl/build-table {} f))]
        (tbl/report *out* table o))
      (tbl/printflush (format "# End Iteration %d\n" i)))))
